package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.QuestionDto;
import uz.pdp.task2.service.QuestionService;

@RestController
@RequestMapping("/api/question")
public class QuestionController {

    @Autowired
    QuestionService questionService;


    /**
     * Method that gets all questions
     *
     * @return ApiResponse
     */
    @GetMapping
    public HttpEntity<?> getAll() {
        ApiResponse apiResponse = questionService.getAll();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    /**
     * Method that gets question by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = questionService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }


    /**
     * Method which adds new Question and edits
     *
     * @param dto QuestionDto
     * @return ApiResponse
     */
    @PostMapping
    public HttpEntity<?> addOrUpdate(@RequestBody QuestionDto dto) {
        ApiResponse apiResponse = questionService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved") ? 201
                : apiResponse.getMessage().equals("Updated") ? 202
                : 409).body(apiResponse);
    }


    /**
     * Method which deletes question by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = questionService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

}
