package uz.pdp.task2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.CategoryDto;
import uz.pdp.task2.service.CategoryService;

@RestController
@RequestMapping("/api/category")
public class CategoryController {


    @Autowired
    CategoryService categoryService;


    /**
     * Method which gets all Categories
     *
     * @return ApiResponse
     */
    @GetMapping
    public HttpEntity<?> getAll() {
        ApiResponse apiResponse = categoryService.getAll();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    /**
     * Method that gets Category by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getById(@PathVariable Long id) {
        ApiResponse apiResponse = categoryService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 404).body(apiResponse);
    }


    /**
     * method which adds new category
     *
     * @param dto CategoryDto
     * @return ApiResponse
     */
    @PostMapping
    public HttpEntity<?> add(@RequestBody CategoryDto dto) {
        ApiResponse apiResponse = categoryService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved") ? 201
                : 409).body(apiResponse);
    }


    /**
     * method which updates new category
     *
     * @param dto CategoryDto
     * @return ApiResponse
     */
    @PutMapping("/update")
    public HttpEntity<?> update(@RequestBody CategoryDto dto) {
        ApiResponse apiResponse = categoryService.addOrUpdate(dto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Updated") ? 202
                : 409).body(apiResponse);
    }


    /**
     * Method which deletes Category by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Long id) {
        ApiResponse apiResponse = categoryService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

}
