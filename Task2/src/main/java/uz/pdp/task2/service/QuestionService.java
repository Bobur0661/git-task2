package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Language;
import uz.pdp.task2.entity.Question;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.QuestionDto;
import uz.pdp.task2.repository.LanguageRepo;
import uz.pdp.task2.repository.QuestionRepo;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionService {


    @Autowired
    QuestionRepo questionRepo;

    @Autowired
    LanguageRepo languageRepo;

    /**
     * Method that gets all questions
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        try {
            List<Question> questionList = questionRepo.findAll();
            return new ApiResponse(true, "Success", questionList);
        } catch (Exception e) {
            return new ApiResponse(false, "Failed");
        }
    }


    /**
     * Method that gets question by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Question> optionalQuestion = questionRepo.findById(id);
//        if (!optionalQuestion.isPresent()) {
//            return new ApiResponse(false, "Not Found");
//        }
//        return new ApiResponse(true, "Success", optionalQuestion.get());
        return optionalQuestion.map(question -> new ApiResponse(true, "Success", question))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which adds new Question and edits
     *
     * @param dto QuestionDto
     * @return ApiResponse
     */
    public ApiResponse addOrUpdate(QuestionDto dto) {
        Question question = new Question();
        if (dto.getId() != null) {
            question = questionRepo.getById(dto.getId());
        }
        Optional<Language> optionalLanguage = languageRepo.findById(dto.getLanguageId());
        if (!optionalLanguage.isPresent()) {
            return new ApiResponse(false, "Language not Found!");
        }
        question.setQuestionText(dto.getQuestionText());
        question.setHasStar(dto.isHasStar());
        question.setLanguage(optionalLanguage.get());
        question.setMethod(dto.getMethod());
        question.setName(dto.getName());
        questionRepo.save(question);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }


    /**
     * Method which deletes question by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            questionRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }
}
