package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.repository.UserRepo;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepo userRepo;

    /**
     * The method that gets all users
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        List<User> users = userRepo.findAll();
        return new ApiResponse(true, "Success", users);
    }


    /**
     * Method that gets user by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<User> optionalUser = userRepo.findById(id);
        return optionalUser.map(user -> new ApiResponse(true, "Success", user))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which adds new User
     *
     * @param user User
     * @return ApiResponse
     */
    public ApiResponse add(User user) {
        boolean existsByEmail = userRepo.existsByEmail(user.getEmail());
        if (existsByEmail) {
            return new ApiResponse(false, "This Email already exists");
        }
        userRepo.save(user);
        return new ApiResponse(true, "Saved");
    }


    /**
     * Method which edits the User by its ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse update(Long id, User updatingUser) {
        Optional<User> optionalUser = userRepo.findById(id);
        if (!optionalUser.isPresent()) {
            return new ApiResponse(false, "Not Found");
        }
        boolean existsByEmailAndIdNot = userRepo.existsByEmailAndIdNot(updatingUser.getEmail(), id);
        if (existsByEmailAndIdNot) {
            return new ApiResponse(false, "This Email already exists");
        }
        User user = optionalUser.get();
        user.setEmail(updatingUser.getEmail());
        user.setPassword(updatingUser.getPassword());
        userRepo.save(user);
        return new ApiResponse(true, "Updated!", user);
    }


    /**
     * Method which deletes user by ID
     *
     * @param id LongX
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            userRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
