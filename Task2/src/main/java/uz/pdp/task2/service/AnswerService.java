package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Answer;
import uz.pdp.task2.entity.Question;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.AnswerDto;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.repository.AnswerRepo;
import uz.pdp.task2.repository.QuestionRepo;
import uz.pdp.task2.repository.UserRepo;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerService {


    @Autowired
    AnswerRepo answerRepo;

    @Autowired
    QuestionRepo questionRepo;

    @Autowired
    UserRepo userRepo;


    /**
     * Method that gets all answers
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        try {
            List<Answer> answerList = answerRepo.findAll();
            return new ApiResponse(true, "Success", answerList);
        } catch (Exception e) {
            return new ApiResponse(false, "Failed");
        }
    }


    /**
     * Method that gets an answer by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Answer> optionalAnswer = answerRepo.findById(id);
        return optionalAnswer.map(answer -> new ApiResponse(true, "Success", answer)).orElseGet(() -> new ApiResponse(false, "Not Found"));

    }


    /**
     * Method which adds a new Answer and edits
     *
     * @param dto AnswerDto
     * @return ApiResponse
     */
    public ApiResponse addOrUpdate(AnswerDto dto) {
        Answer answer = new Answer();
        if (dto.getId() != null) {
            answer = answerRepo.getById(dto.getId());
        }
        Optional<Question> questionOptional = questionRepo.findById(dto.getQuestionId());
        if (!questionOptional.isPresent()) {
            return new ApiResponse(false, "Question not Found!");
        }
        Optional<User> optionalUser = userRepo.findById(dto.getUserId());
        if (!optionalUser.isPresent()) {
            return new ApiResponse(false, "User not Found!");
        }
        answer.setAnswerText(dto.getAnswerText());
        answer.setCorrect(dto.isCorrect());
        answer.setUser(optionalUser.get());
        answer.setQuestion(questionOptional.get());
        answerRepo.save(answer);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }


    /**
     * Method which deletes answer by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            answerRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
