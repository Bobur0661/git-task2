package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Example;
import uz.pdp.task2.entity.Question;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.ExampleDto;
import uz.pdp.task2.repository.ExampleRepo;
import uz.pdp.task2.repository.QuestionRepo;

import java.util.List;
import java.util.Optional;

@Service
public class ExampleService {


    @Autowired
    ExampleRepo exampleRepo;

    @Autowired
    QuestionRepo questionRepo;


    /**
     * Method that gets all examples
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        try {
            List<Example> exampleList = exampleRepo.findAll();
            return new ApiResponse(true, "Success", exampleList);
        } catch (Exception e) {
            return new ApiResponse(false, "Failed");
        }
    }


    /**
     * Method that gets example by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Example> optionalExample = exampleRepo.findById(id);
//        if (!optionalExample.isPresent()) {
//            return new ApiResponse(false, "Not Found");
//        }
//        return new ApiResponse(true, "Success", optionalExample.get());
        return optionalExample.map(example -> new ApiResponse(true, "Success", example))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which adds new Example and edits
     *
     * @param dto ExampleDto
     * @return ApiResponse
     */
    public ApiResponse addOrUpdate(ExampleDto dto) {
        Example example = new Example();
        if (dto.getId() != null) {
            example = exampleRepo.getById(dto.getId());
        }
        Optional<Question> questionOptional = questionRepo.findById(dto.getQuestionId());
        if (!questionOptional.isPresent()) {
            return new ApiResponse(false, "Question not Found!");
        }
        example.setText(dto.getText());
        example.setQuestion(questionOptional.get());
        exampleRepo.save(example);
        return new ApiResponse(true, dto.getId() != null ? "Updated" : "Saved");
    }


    /**
     * Method which deletes example by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            exampleRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }

}
