package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task2.entity.Language;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.repository.LanguageRepo;

import java.util.List;
import java.util.Optional;

@Service
public class LanguageService {


    @Autowired
    LanguageRepo languageRepo;


    /**
     * The method that gets all languages
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        List<Language> languages = languageRepo.findAll();
        return languages != null ? new ApiResponse(true, "Success", languages)
                : new ApiResponse(false, "Failed");
    }


    /**
     * Method that gets language by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Language> optionalLanguage = languageRepo.findById(id);
        return optionalLanguage.map(language -> new ApiResponse(true, "Success", language))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * Method which add new Language
     *
     * @param language Language
     * @return ApiResponse
     */
    public ApiResponse add(Language language) {
        boolean existsByName = languageRepo.existsByName(language.getName());
        if (existsByName) {
            return new ApiResponse(false, "This Language already exists");
        }
        languageRepo.save(language);
        return new ApiResponse(true, "Saved");
    }


    /**
     * Method which edits the Language by its ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse update(Long id, Language updatingLanguage) {
        Optional<Language> optionalLanguage = languageRepo.findById(id);
        if (!optionalLanguage.isPresent()) {
            return new ApiResponse(false, "Not Found");
        }
        Language language = optionalLanguage.get();
        language.setName(updatingLanguage.getName());
        languageRepo.save(language);
        return new ApiResponse(true, "Updated!", language);
    }


    /**
     * Method which deletes language by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            languageRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }


}
