package uz.pdp.task2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.krb5.internal.APOptions;
import uz.pdp.task2.entity.Category;
import uz.pdp.task2.entity.Language;
import uz.pdp.task2.entity.User;
import uz.pdp.task2.payload.ApiResponse;
import uz.pdp.task2.payload.CategoryDto;
import uz.pdp.task2.repository.CategoryRepo;
import uz.pdp.task2.repository.LanguageRepo;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    CategoryRepo categoryRepo;

    @Autowired
    LanguageRepo languageRepo;


    /**
     * Method which gets all Categories
     *
     * @return ApiResponse
     */
    public ApiResponse getAll() {
        try {
            List<Category> all = categoryRepo.findAll();
            return new ApiResponse(true, "Success", all);
        } catch (Exception e) {
            return new ApiResponse(false, "Cannot Find");
        }
    }


    /**
     * Method that gets Category by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse getById(Long id) {
        Optional<Category> optionalCategory = categoryRepo.findById(id);
        return optionalCategory.map(user -> new ApiResponse(true, "Success", optionalCategory))
                .orElseGet(() -> new ApiResponse(false, "Not Found"));
    }


    /**
     * method which creates and updates new category
     *
     * @param dto CategoryDto
     * @return ApiResponse
     */
    public ApiResponse addOrUpdate(CategoryDto dto) {
        boolean existsByName = categoryRepo.existsByName(dto.getName());
        for (Language language : dto.getLanguages()) {
            Optional<Language> optionalLanguage = languageRepo.findById(language.getId());
            if (!optionalLanguage.isPresent()) {
                return new ApiResponse(false, "Cannot find Language!");
            }
        }
        if (dto.getId() == null) {
            if (existsByName) {
                return new ApiResponse(false, "The category already exists");
            }
            Category category = new Category();
            category.setName(dto.getName());
            category.setDescription(dto.getDescription());
            category.setLanguages(dto.getLanguages());
            categoryRepo.save(category);
            return new ApiResponse(true, "Saved");
        } else {
            Optional<Category> optionalCategory = categoryRepo.findById(dto.getId());
            Category editCategory = optionalCategory.get();
            if (!optionalCategory.isPresent()) {
                return new ApiResponse(false, "Cannot find category!");
            } else if (optionalCategory.get().getName().equals(dto.getName())) {
                editCategory.setDescription(dto.getDescription());
                editCategory.setLanguages(dto.getLanguages());
                categoryRepo.save(editCategory);
                return new ApiResponse(false, "Updated");
            } else {
                editCategory.setName(dto.getName());
                editCategory.setDescription(dto.getDescription());
                editCategory.setLanguages(dto.getLanguages());
                boolean existsByNameAndIdNot = categoryRepo.existsByNameAndIdNot(dto.getName(), dto.getId());
                if (existsByNameAndIdNot) {
                    return new ApiResponse(false, "The category already exists!");
                }
                categoryRepo.save(editCategory);
                return new ApiResponse(true, "Updated");
            }
        }
    }


    /**
     * Method which deletes Category by ID
     *
     * @param id Long
     * @return ApiResponse
     */
    public ApiResponse delete(Long id) {
        try {
            categoryRepo.deleteById(id);
            return new ApiResponse(true, "Deleted!");
        } catch (Exception e) {
            return new ApiResponse(false, "Error on Deleting!");
        }
    }

}
