package uz.pdp.task2.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task2.entity.Question;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExampleDto {

    private Long id;
    private String text;
    private Long questionId;

}
