package uz.pdp.task2.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task2.entity.Language;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDto {

    private Long id;
    private String name;
    private String questionText;
    private String solution;
    private String showHint;
    private String method;
    private boolean hasStar;
    private Long languageId;

}
