package uz.pdp.task2.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task2.entity.Question;
import uz.pdp.task2.entity.User;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerDto {


    private Long id;
    private String answerText;
    private boolean correct;
    private Long questionId;
    private Long userId;


}
