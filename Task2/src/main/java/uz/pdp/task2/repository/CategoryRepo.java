package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.Category;

public interface CategoryRepo extends JpaRepository<Category, Long> {


    boolean existsByName(String name);


    boolean existsByNameAndIdNot(String name, Long id);

}
