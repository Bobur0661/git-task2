package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.Language;

public interface LanguageRepo extends JpaRepository<Language, Long> {

    boolean existsByName(String name);


}
