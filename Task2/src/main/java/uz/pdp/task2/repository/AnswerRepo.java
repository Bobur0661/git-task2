package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.Answer;

public interface AnswerRepo extends JpaRepository<Answer, Long> {
}
