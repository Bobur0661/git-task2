package uz.pdp.task2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task2.entity.Question;

public interface QuestionRepo extends JpaRepository<Question, Long> {
}
